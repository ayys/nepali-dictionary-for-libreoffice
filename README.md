# लिब्रेअफिसको मागि नेपाली शब्दकोश 

This dictionary contains words from multiple sources including
the [National Nepali Corpus compiled by Madan Puraskar Pustakalaya](http://web.archive.org/web/20150826233511/http://www.bhashasanchar.org:80/aboutnnc.php), 
[Nepali Vrihat Shabdakosh published by Pragya Pratisthan](https://ia802701.us.archive.org/23/items/Nepalibrihatsabdakos-pragyapratisthan-7thedition_1UploadedBy/Nepalibrihatsabdakos-pragyapratisthan-7thedition_1UploadedByKirandhakal.blogspot.com.pdf).
It also includes numbers, characters and slangs.

Please note that this is not an exhaustive list,
so feel free to send merge requeusts or create issues with new words.

The dictionary currently has 169277 words in total.

Please note the license of the project before using the dictionary in
a project. Create an issue if you are not sure if the dictionary can
be used in your project.

# How to Install

```bash
git clone https://gitlab.com/ayys/nepali-dictionary-for-libreoffice.git
cd nepali-dictionary-for-libreoffice
cp नेपाली.dic $HOME/.config/libreoffice/4/user/wordbook

```

Now restart libreoffice and try typing in Nepali.
